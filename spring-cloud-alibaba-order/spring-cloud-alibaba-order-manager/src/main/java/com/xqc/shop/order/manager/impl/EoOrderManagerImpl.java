package com.xqc.shop.order.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.xqc.shop.order.mapper.EoOrderMapper;
import com.xqc.shop.order.manager.EoOrderManager;
import com.xqc.shop.order.model.EoOrderModel;

@Component
@Transactional
public class EoOrderManagerImpl implements EoOrderManager{

	@Autowired
	private EoOrderMapper eoOrderMapper;
	
	public int insert(EoOrderModel orderModel) {
		return this.eoOrderMapper.insert(orderModel);
	}

}
