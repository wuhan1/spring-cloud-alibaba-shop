package com.xqc.shop.order.controller;

import com.xqc.shop.order.service.EoOrderService;
import com.xqc.shop.order.bean.EoOrderBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class Controller {

    @Autowired
    private EoOrderService eoOrderService;

    @GetMapping("/add")
    public boolean test(){
        EoOrderBean eoOrderBean = new EoOrderBean();
        eoOrderBean.setStockId(1L);
        eoOrderBean.setUserId(1L);
        return this.eoOrderService.insert(eoOrderBean);
    }
}
