package com.xqc.shop.order.service.impl;

import com.xqc.shop.order.bean.EoOrderBean;
import com.xqc.shop.order.manager.EoOrderManager;
import com.xqc.shop.order.model.EoOrderModel;
import com.xqc.shop.order.service.EoOrderService;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EoOrderServiceImpl implements  EoOrderService{

    @Autowired
    private EoOrderManager eoOrderManager;

    @Override
    public Object getIp() {
        return "7007";
    }

    @Override
    public boolean insert(EoOrderBean eoOrderBean) {
        EoOrderModel eoOrderModel = new EoOrderModel();
        BeanUtils.copyProperties(eoOrderBean,eoOrderModel);
        return this.eoOrderManager.insert(eoOrderModel) > 0;
    }
}
