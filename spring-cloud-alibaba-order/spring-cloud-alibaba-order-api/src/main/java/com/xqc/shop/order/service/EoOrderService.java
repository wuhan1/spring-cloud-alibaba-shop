package com.xqc.shop.order.service;

import com.xqc.shop.order.bean.EoOrderBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("order-service-dev")
public interface EoOrderService {

    @GetMapping(value="/order/getIp",produces={"application/json; charset=UTF-8"})
    Object getIp();

    @PostMapping(value="/order/insert",produces={"application/json; charset=UTF-8"})
    boolean insert(@RequestBody EoOrderBean eoOrderBean);
}
