package com.xqc.shop.order.mapper;

import com.xqc.shop.order.model.EoOrderModel;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 作者 Gavin.Dai/SNSSZ00036
* @dateTime 创建时间：2020年8月6日 下午2:28:50
* @version V1.0.0
* 类说明
*/
@Mapper
public interface EoOrderMapper {

	int insert(EoOrderModel orderModel);
}
