--- 锁名称
local lockName = KEYS[1]
--- 锁的值
local lockValue=ARGV[1]
--尝试枷锁
local value = redis.call('get',lockName)
--是否获取到锁
if value ==lockValue then
--获取到了 锁设置过期时间
	redis.call('del',lockName)
	return 1
end
--返回结果
return 0