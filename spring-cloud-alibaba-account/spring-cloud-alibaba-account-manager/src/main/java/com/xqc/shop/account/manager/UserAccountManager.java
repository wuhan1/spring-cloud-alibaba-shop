package com.xqc.shop.account.manager;

import com.xqc.shop.account.model.UserAccountModel;

public interface UserAccountManager {

    boolean insert(UserAccountModel accountModel);

    boolean updateMoney(Integer userId);
}
