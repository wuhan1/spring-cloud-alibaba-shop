package com.xqc.shop.account.manager.impl;

import com.xqc.shop.account.manager.UserAccountManager;
import com.xqc.shop.account.mapper.UserAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.xqc.shop.account.model.UserAccountModel;

@Component
@Transactional
public class UserAccountManagerImpl implements UserAccountManager {

	@Autowired
	private UserAccountMapper userAccountMapper;
	
	@Override
	public boolean insert(UserAccountModel accountModel) {
		return this.userAccountMapper.insert(accountModel) > 0;
	}

	@Override
	public boolean updateMoney(Integer userId) {
		return this.userAccountMapper.updateMoney(userId) >0;
	}

}
