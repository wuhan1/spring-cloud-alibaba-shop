package com.xqc.shop.account.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * 项目启动时加载缓存
 * @author Administrator
 *
 */
@Component
@Order(value=1)//设置加载顺序
public class RedisCacheLoader implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(RedisCacheLoader.class);
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	@Override
	public void run(String... args) throws Exception {
		logger.info("=============加载缓存数据到redis开始=============");
		logger.info("=============加载缓存数据到redis结束=============");
	}

}
