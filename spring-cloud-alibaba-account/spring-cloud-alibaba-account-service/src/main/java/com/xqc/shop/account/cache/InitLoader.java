package com.xqc.shop.account.cache;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class InitLoader {
	
	private static final Logger logger = LoggerFactory.getLogger(InitLoader.class);

	@PostConstruct
	private void init() {
		logger.info("第三种方式启动加载文件");
	}
}
