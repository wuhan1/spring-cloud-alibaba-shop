package com.xqc.shop.account.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xqc.shop.account.api.UserAccountService;

@RestController
@RequestMapping("/account")
public class UserAccountController {

	@Autowired
	private UserAccountService userAccountService;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@GetMapping("/getIp")
	public Object getIp() {
		Map<String,String> dataMap = new HashMap<>();
		dataMap.put("IP", "localhost");
		dataMap.put("PORT", "7003");
		dataMap.put("REMARK", "测试7003");
		return dataMap;
	}
	
	@GetMapping("/updateMoney")
	public boolean updateMoney(Integer userId) {
		return this.userAccountService.updateMoney(userId);
	}

	@GetMapping("/createOrder")
	public boolean createOrder() {
		return this.userAccountService.createOrder();
	}
	
}
