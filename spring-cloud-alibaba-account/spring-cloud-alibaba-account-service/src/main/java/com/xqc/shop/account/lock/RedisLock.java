package com.xqc.shop.account.lock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * redis+lua分布式锁
 * @author Administrator
 *
 */
@Component
public class RedisLock {
	
	private RedisLock() {}

	private static final Long RELEASE_SUCCESS = 1L;
	/**
	 * 持有锁时间，10s
	 */
	private static final long DEFAULT_TIMEOUT = 1000 * 10;
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	public static boolean lock(String key, String value, long timeout) {
//		return redisTemplate.
		return true;
	}
	
}
