--- 锁名称
local lockName = KEYS[1]
--- 锁的值
local lockValue=ARGV[1]
--过期时间
local timeout=tonumber(ARGV[2])
--尝试枷锁
local flag = redis.call('setnx',lockName,lockValue)
--是否获取到锁
if flag==1 then
--获取到了 锁设置过期时间
	redis.call('expire',lockName,timeout)
else
-- 如果value相同，则认为是同一个线程的请求，则认为重入锁
	local value = redis.call('get',lockName);
	if value == lockValue then
		flag = 1;
		-- 重新设置锁过期时间
		redis.call('expire',lockName,timeout)
	end
end
--返回结果
return flag