/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50528
 Source Host           : localhost:3306
 Source Schema         : slf4j

 Target Server Type    : MySQL
 Target Server Version : 50528
 File Encoding         : 65001

 Date: 09/08/2020 20:56:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for seckill
-- ----------------------------
DROP TABLE IF EXISTS `seckill`;
CREATE TABLE `seckill`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TOTAL` int(2) NULL DEFAULT NULL,
  `START_TIME` datetime NULL DEFAULT NULL,
  `END_TIME` datetime NULL DEFAULT NULL,
  `REMARK` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_TIME` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of seckill
-- ----------------------------
INSERT INTO `seckill` VALUES (1, 0, '2020-08-07 23:38:23', '2020-08-10 23:38:29', '秒杀活动', '2020-08-08 23:38:40');

-- ----------------------------
-- Table structure for seckill_detail
-- ----------------------------
DROP TABLE IF EXISTS `seckill_detail`;
CREATE TABLE `seckill_detail`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NULL DEFAULT NULL,
  `SECKILL_ID` int(11) NULL DEFAULT NULL,
  `CREATE_TIME` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 524 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of seckill_detail
-- ----------------------------
INSERT INTO `seckill_detail` VALUES (520, 2011947613, 1, '2020-08-09 16:31:48');
INSERT INTO `seckill_detail` VALUES (521, 1021168541, 1, '2020-08-09 16:31:56');
INSERT INTO `seckill_detail` VALUES (522, 636696149, 1, '2020-08-09 16:31:57');
INSERT INTO `seckill_detail` VALUES (523, 19957417, 1, '2020-08-09 16:31:57');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `ID` bigint(32) NOT NULL,
  `USERNAME` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PASSWORD` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CH_NAME` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EMAIL` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MOBILE` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BIRTHDAY` datetime NULL DEFAULT NULL,
  `INTEGRAL` int(12) NULL DEFAULT 15,
  `DISABLED` int(1) NULL DEFAULT -1 COMMENT '-1:未认证0:冻结1:正常',
  `IS_ADMIN` int(1) NULL DEFAULT 0 COMMENT '1:管理员0:普通用户',
  `GRADE` int(11) NULL DEFAULT 1 COMMENT '会员等级',
  `PICTURE` longblob NULL,
  `IP` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '获取注册者的ip',
  `CREATE_DATE` datetime NULL DEFAULT NULL COMMENT '注册时间',
  `OUT_DATE` datetime NULL DEFAULT NULL COMMENT '邮箱找回密码过期时间',
  `VALIDATA_CODE` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '找回密码唯一标示',
  `IS_ACTIVATE` int(1) NULL DEFAULT 0 COMMENT '0:激活1:未激活',
  `SEX` int(1) NULL DEFAULT 0 COMMENT '性别0:保密:1男:2女',
  `PROVINCE` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省',
  `CITY` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '市',
  `INDUSTRY` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行业',
  `JOB` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '职业',
  `BLOG_ID` int(11) NULL DEFAULT NULL COMMENT '博客ID',
  `IS_OPEN_BLOG` int(1) NULL DEFAULT 0 COMMENT '是否开通博客0：否1：是',
  `IS_ALLOW_AT` int(1) NULL DEFAULT 0 COMMENT '是否允许艾特我0：任何人1：好友或互相关注的人2：禁止任何人',
  `IS_ALLOW_SEND_MSG` int(1) NULL DEFAULT 0 COMMENT '谁可以给我发消息0：任何人1：好友或互相关注的人2：禁止任何人',
  `IS_SUBSCRIBE_MSG` int(1) NULL DEFAULT 0 COMMENT '是否订阅消息0：是1：否',
  `INVITATION_CODE` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邀请码',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '123456', NULL, NULL, NULL, NULL, 15, -1, 0, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, '', NULL, 0, 0, 0, 0, NULL);
INSERT INTO `user` VALUES (2, 'test', '123456', NULL, NULL, NULL, NULL, 15, -1, 0, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, '', NULL, 0, 0, 0, 0, NULL);
INSERT INTO `user` VALUES (3, 'zhangsn', '123456', NULL, NULL, NULL, NULL, 15, -1, 0, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, '', NULL, 0, 0, 0, 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;
