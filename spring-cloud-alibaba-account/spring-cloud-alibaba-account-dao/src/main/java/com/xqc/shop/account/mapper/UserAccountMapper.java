package com.xqc.shop.account.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.xqc.shop.account.model.UserAccountModel;
/**
* @author 作者 Gavin.Dai/SNSSZ00036
* @dateTime 创建时间：2020年8月6日 下午2:28:50
* @version V1.0.0
* 类说明
*/
@Mapper
public interface UserAccountMapper {

	int insert(UserAccountModel orderModel);
	
	int updateMoney(Integer userId);
}
