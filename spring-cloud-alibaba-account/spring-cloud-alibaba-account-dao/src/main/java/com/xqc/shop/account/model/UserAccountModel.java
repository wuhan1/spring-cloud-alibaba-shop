package com.xqc.shop.account.model;

import java.io.Serializable;

public class UserAccountModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Double money;
    private Double usedMoney;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Double getUsedMoney() {
        return usedMoney;
    }

    public void setUsedMoney(Double usedMoney) {
        this.usedMoney = usedMoney;
    }

}
