package com.xqc.shop.account.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

//@FeignClient(value="${spring.application.name}")
@FeignClient(value="account-service-dev")
public interface  UserAccountService {
	
	@GetMapping(value="/account/updateMoney",produces={"application/json; charset=UTF-8"})
	 boolean updateMoney(Integer userId);
	
	@GetMapping(value="/account/getIp",produces={"application/json; charset=UTF-8"})
	 Object getIp();
	
	
	@GetMapping(value="/account/createOrder",produces={"application/json; charset=UTF-8"})
	 boolean createOrder();
	
}
