package com.xqc.shop.stock.manager.impl;

import com.xqc.shop.stock.mapper.EoStockMapper;
import com.xqc.shop.stock.model.EoStockModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.xqc.shop.stock.manager.EoStockManager;

@Component
@Transactional
public class EoStockManagerImpl implements EoStockManager {
	
	@Autowired
	private EoStockMapper eoStockMapper;
	

	@Override
	public boolean insert(EoStockModel stockModel) {
		return this.eoStockMapper.insert(stockModel) > 0;
	}


	@Override
	public boolean updateStock(Integer stockId) {
		return this.eoStockMapper.updateStock(stockId) > 0;
	}

}
