package com.xqc.shop.stock.manager;

import com.xqc.shop.stock.model.EoStockModel;

public interface EoStockManager {

	boolean insert(EoStockModel stockModel);
	
	boolean updateStock(Integer stockId);
}
