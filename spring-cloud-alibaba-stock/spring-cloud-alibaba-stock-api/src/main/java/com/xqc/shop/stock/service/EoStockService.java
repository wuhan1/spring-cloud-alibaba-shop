package com.xqc.shop.stock.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//@FeignClient(value="${spring.application.name}")
@FeignClient(value="stock-service-dev")
public interface  EoStockService {
	
	@GetMapping(value="/stock/insertStock",produces={"application/json; charset=UTF-8"})
	 boolean insertStock();
	
	@GetMapping(value="/stock/updateStock",produces={"application/json; charset=UTF-8"})
	boolean updateStock(@RequestParam("stockId") Integer stockId);
	
	@GetMapping(value="/stock/getIp",produces={"application/json; charset=UTF-8"})
	 Object getIp();
	
}
