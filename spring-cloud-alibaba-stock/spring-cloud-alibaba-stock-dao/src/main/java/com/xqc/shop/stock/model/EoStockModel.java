package com.xqc.shop.stock.model;

import java.io.Serializable;

public class EoStockModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Double qty;
	private Double usedQty;
	private Double rediusQty;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getUsedQty() {
		return usedQty;
	}

	public void setUsedQty(Double usedQty) {
		this.usedQty = usedQty;
	}

	public Double getRediusQty() {
		return rediusQty;
	}

	public void setRediusQty(Double rediusQty) {
		this.rediusQty = rediusQty;
	}

}
