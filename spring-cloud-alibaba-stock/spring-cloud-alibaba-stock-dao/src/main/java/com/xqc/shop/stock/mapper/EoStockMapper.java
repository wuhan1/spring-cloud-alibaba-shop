package com.xqc.shop.stock.mapper;

import com.xqc.shop.stock.model.EoStockModel;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 作者 Gavin.Dai/SNSSZ00036
* @dateTime 创建时间：2020年8月6日 下午2:28:50
* @version V1.0.0
* 类说明
*/
@Mapper
public interface EoStockMapper {

	int insert(EoStockModel stockModel);
	
	int updateStock(Integer stockId);
}
