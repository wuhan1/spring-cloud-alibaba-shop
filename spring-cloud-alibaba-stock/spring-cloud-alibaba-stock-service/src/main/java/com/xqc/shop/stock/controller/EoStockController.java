package com.xqc.shop.stock.controller;

import com.xqc.shop.stock.service.EoStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stock")
public class EoStockController {

    @Autowired
    private EoStockService eoStockService;

    @GetMapping(value="/updateStock")
    public boolean updateStock(@RequestParam("stockId") Integer stockId){
        return this.eoStockService.updateStock(stockId);
    }

}
