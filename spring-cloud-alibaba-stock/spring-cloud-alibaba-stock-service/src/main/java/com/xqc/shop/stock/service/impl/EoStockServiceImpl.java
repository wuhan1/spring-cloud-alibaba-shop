package com.xqc.shop.stock.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RestController;

import com.xqc.shop.stock.manager.EoStockManager;
import com.xqc.shop.stock.model.EoStockModel;
import com.xqc.shop.stock.service.EoStockService;


@RestController
public class EoStockServiceImpl implements EoStockService {

	@Autowired
	private EoStockManager eoStockManager;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	public String say() {
		return "say";
	}
	
	
	@Override
	public Object getIp() {
		Map<String,String> dataMap = new HashMap<>();
		dataMap.put("IP", "localhost");
		dataMap.put("PORT", "7003");
		dataMap.put("REMARK", "测试7003");
		return dataMap;
	}


	@Override
	public boolean insertStock() {
		EoStockModel stockModel  = new  EoStockModel();
		return this.eoStockManager.insert(stockModel);
	}


	@Override
	public boolean updateStock(Integer stockId) {
		return this.eoStockManager.updateStock(stockId);
	}
	
}
